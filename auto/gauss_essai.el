(TeX-add-style-hook
 "gauss_essai"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "12pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "height=250mm" "width=183mm") ("fontenc" "T1")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art12"
    "geometry"
    "fontenc"
    "gauss"))
 :latex)

