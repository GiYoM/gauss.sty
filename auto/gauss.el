(TeX-add-style-hook
 "gauss"
 (lambda ()
   (TeX-run-style-hooks
    "array"
    "amsmath"
    "xcolor"
    "fancyvrb")
   (TeX-add-symbols
    "sys")
   (LaTeX-add-environments)
   (LaTeX-add-counters
    "syst"))
 :latex)

