# Gauss.sty

LaTeX extension to solve linear systems 

## Requirements
Python >= 3.6 et LaTeX2e 

## Versions

- 1.00 (28/12/2021)
- 1.01 (02/02/2022) : `\sys` argument removed
- 1.02 (06/02/2022) : `gosse` function removed

## Usage

Make your own LaTeX file. Type your matrix as a list of lists. 
For instance to solve 
```math
   \left\{
     \begin{matrix}
            2x-3y+6z+2t=4\\
       3x+y-2z+t=1\\
      2x+y+z-3t=2
    \end{matrix}\right.
```


and

```math
   \left\{
     \begin{matrix}
            2x-3y=6\\
       3x+y=-2\\
      2x+y=1\\
      2x+3y=4
    \end{matrix}\right.
```


```latex
\documentclass[12pt]{article}
\usepackage[height=250mm,width=183mm]{geometry}
\usepackage[T1]{fontenc}
\usepackage{gauss}

\begin{document}

 \begin{gauss}
  [
  [2, -3,  6,  2, 4],
  [3,  1, -2,  1, 1],
  [2,  1,  1, -3, 2]
  ]
 \end{gauss}

 \begin{gauss}
  [
  [2, -3,  6],
  [3,  1, -2],
  [2,  1,  1],
  [2,  3,  4]
  ]
 \end{gauss}

\end{document}
```

You need to compile with `--shell-escape` option and you get this :

 ![this](gausspdf.png)

## Acknowledgment
Thanks to [Jannes Höke](https://github.com/jh0ker/gauss_latex)

## License
 This package is distributed under the terms of the LaTeX Project Public License (LPPL)

